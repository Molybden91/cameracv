/*******************************************************************************
	Generated by: DLL to C version 3.02
	Date: 2018-6-18
	Description: The implementation code for StreamReader.dll.
	Website: http://www.dll-decompiler.com
	Technical Support: support@dll-decompiler.com
*******************************************************************************/

#include "stdafx.h"
#include "StreamReader.h"


HMODULE g_hStreamReader;
BOOL (WINAPI *StreamReader_DllEntryPoint)(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved);

void* __stdcall StreamReader_RVA(DWORD rvaAddr)
{
	if(rvaAddr==0)
		return g_hStreamReader;
	if(rvaAddr >= 0x1000 && rvaAddr < 0x18000)
		return &StreamReader_text[rvaAddr - 0x1000];
	if(rvaAddr >= 0x18000 && rvaAddr < 0x1A000)
		return &StreamReader_rdata[rvaAddr - 0x18000];
	if(rvaAddr >= 0x1A000 && rvaAddr < 0x21000)
		return &StreamReader_data[rvaAddr - 0x1A000];
	if(rvaAddr >= 0x21000 && rvaAddr < 0x22000)
		return &StreamReader_idata[rvaAddr - 0x21000];
	if(rvaAddr > 0 && rvaAddr < 0x1000)
		return (UCHAR*)g_hStreamReader + rvaAddr;

	return NULL;
}

BOOL StreamReader_Init()
{
	HMODULE hDll;
	DWORD oldProtect;
	void (*fInitData)(void*);

	g_hStreamReader = GetModuleHandle(0);

	oldProtect = PAGE_EXECUTE_READWRITE;
	VirtualProtect(StreamReader_text,sizeof(StreamReader_text),PAGE_EXECUTE_READWRITE,&oldProtect);

	hDll = ::LoadLibraryA("KERNEL32.dll");
	if(!hDll)
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x211BC) = ::GetProcAddress(hDll,"RtlUnwind");
	if(!*(FARPROC*)StreamReader_RVA(0x211BC))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x211C0) = ::GetProcAddress(hDll,"GetTimeZoneInformation");
	if(!*(FARPROC*)StreamReader_RVA(0x211C0))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x211C4) = ::GetProcAddress(hDll,"GetSystemTime");
	if(!*(FARPROC*)StreamReader_RVA(0x211C4))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x211C8) = ::GetProcAddress(hDll,"GetLocalTime");
	if(!*(FARPROC*)StreamReader_RVA(0x211C8))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x211CC) = ::GetProcAddress(hDll,"GetCommandLineA");
	if(!*(FARPROC*)StreamReader_RVA(0x211CC))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x211D0) = ::GetProcAddress(hDll,"GetVersion");
	if(!*(FARPROC*)StreamReader_RVA(0x211D0))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x211D4) = ::GetProcAddress(hDll,"HeapFree");
	if(!*(FARPROC*)StreamReader_RVA(0x211D4))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x211D8) = ::GetProcAddress(hDll,"WideCharToMultiByte");
	if(!*(FARPROC*)StreamReader_RVA(0x211D8))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x211DC) = ::GetProcAddress(hDll,"HeapAlloc");
	if(!*(FARPROC*)StreamReader_RVA(0x211DC))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x211E0) = ::GetProcAddress(hDll,"GetCurrentThreadId");
	if(!*(FARPROC*)StreamReader_RVA(0x211E0))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x211E4) = ::GetProcAddress(hDll,"TlsSetValue");
	if(!*(FARPROC*)StreamReader_RVA(0x211E4))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x211E8) = ::GetProcAddress(hDll,"TlsAlloc");
	if(!*(FARPROC*)StreamReader_RVA(0x211E8))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x211EC) = ::GetProcAddress(hDll,"TlsFree");
	if(!*(FARPROC*)StreamReader_RVA(0x211EC))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x211F0) = ::GetProcAddress(hDll,"SetLastError");
	if(!*(FARPROC*)StreamReader_RVA(0x211F0))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x211F4) = ::GetProcAddress(hDll,"TlsGetValue");
	if(!*(FARPROC*)StreamReader_RVA(0x211F4))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x211F8) = ::GetProcAddress(hDll,"GetLastError");
	if(!*(FARPROC*)StreamReader_RVA(0x211F8))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x211FC) = ::GetProcAddress(hDll,"GetCurrentThread");
	if(!*(FARPROC*)StreamReader_RVA(0x211FC))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21200) = ::GetProcAddress(hDll,"EnterCriticalSection");
	if(!*(FARPROC*)StreamReader_RVA(0x21200))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21204) = ::GetProcAddress(hDll,"LeaveCriticalSection");
	if(!*(FARPROC*)StreamReader_RVA(0x21204))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21208) = ::GetProcAddress(hDll,"ExitProcess");
	if(!*(FARPROC*)StreamReader_RVA(0x21208))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x2120C) = ::GetProcAddress(hDll,"TerminateProcess");
	if(!*(FARPROC*)StreamReader_RVA(0x2120C))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21210) = ::GetProcAddress(hDll,"GetCurrentProcess");
	if(!*(FARPROC*)StreamReader_RVA(0x21210))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21214) = ::GetProcAddress(hDll,"SetHandleCount");
	if(!*(FARPROC*)StreamReader_RVA(0x21214))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21218) = ::GetProcAddress(hDll,"GetStdHandle");
	if(!*(FARPROC*)StreamReader_RVA(0x21218))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x2121C) = ::GetProcAddress(hDll,"GetFileType");
	if(!*(FARPROC*)StreamReader_RVA(0x2121C))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21220) = ::GetProcAddress(hDll,"GetStartupInfoA");
	if(!*(FARPROC*)StreamReader_RVA(0x21220))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21224) = ::GetProcAddress(hDll,"DeleteCriticalSection");
	if(!*(FARPROC*)StreamReader_RVA(0x21224))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21228) = ::GetProcAddress(hDll,"GetModuleFileNameA");
	if(!*(FARPROC*)StreamReader_RVA(0x21228))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x2122C) = ::GetProcAddress(hDll,"FreeEnvironmentStringsA");
	if(!*(FARPROC*)StreamReader_RVA(0x2122C))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21230) = ::GetProcAddress(hDll,"FreeEnvironmentStringsW");
	if(!*(FARPROC*)StreamReader_RVA(0x21230))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21234) = ::GetProcAddress(hDll,"GetEnvironmentStrings");
	if(!*(FARPROC*)StreamReader_RVA(0x21234))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21238) = ::GetProcAddress(hDll,"GetEnvironmentStringsW");
	if(!*(FARPROC*)StreamReader_RVA(0x21238))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x2123C) = ::GetProcAddress(hDll,"GetModuleHandleA");
	if(!*(FARPROC*)StreamReader_RVA(0x2123C))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21240) = ::GetProcAddress(hDll,"GetEnvironmentVariableA");
	if(!*(FARPROC*)StreamReader_RVA(0x21240))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21244) = ::GetProcAddress(hDll,"GetVersionExA");
	if(!*(FARPROC*)StreamReader_RVA(0x21244))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21248) = ::GetProcAddress(hDll,"HeapDestroy");
	if(!*(FARPROC*)StreamReader_RVA(0x21248))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x2124C) = ::GetProcAddress(hDll,"HeapCreate");
	if(!*(FARPROC*)StreamReader_RVA(0x2124C))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21250) = ::GetProcAddress(hDll,"VirtualFree");
	if(!*(FARPROC*)StreamReader_RVA(0x21250))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21254) = ::GetProcAddress(hDll,"WriteFile");
	if(!*(FARPROC*)StreamReader_RVA(0x21254))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21258) = ::GetProcAddress(hDll,"VirtualAlloc");
	if(!*(FARPROC*)StreamReader_RVA(0x21258))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x2125C) = ::GetProcAddress(hDll,"HeapReAlloc");
	if(!*(FARPROC*)StreamReader_RVA(0x2125C))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21260) = ::GetProcAddress(hDll,"IsBadWritePtr");
	if(!*(FARPROC*)StreamReader_RVA(0x21260))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21264) = ::GetProcAddress(hDll,"InitializeCriticalSection");
	if(!*(FARPROC*)StreamReader_RVA(0x21264))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21268) = ::GetProcAddress(hDll,"FatalAppExitA");
	if(!*(FARPROC*)StreamReader_RVA(0x21268))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x2126C) = ::GetProcAddress(hDll,"SetUnhandledExceptionFilter");
	if(!*(FARPROC*)StreamReader_RVA(0x2126C))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21270) = ::GetProcAddress(hDll,"IsBadReadPtr");
	if(!*(FARPROC*)StreamReader_RVA(0x21270))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21274) = ::GetProcAddress(hDll,"IsBadCodePtr");
	if(!*(FARPROC*)StreamReader_RVA(0x21274))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21278) = ::GetProcAddress(hDll,"UnhandledExceptionFilter");
	if(!*(FARPROC*)StreamReader_RVA(0x21278))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x2127C) = ::GetProcAddress(hDll,"InterlockedDecrement");
	if(!*(FARPROC*)StreamReader_RVA(0x2127C))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21280) = ::GetProcAddress(hDll,"InterlockedIncrement");
	if(!*(FARPROC*)StreamReader_RVA(0x21280))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21284) = ::GetProcAddress(hDll,"GetCPInfo");
	if(!*(FARPROC*)StreamReader_RVA(0x21284))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21288) = ::GetProcAddress(hDll,"GetACP");
	if(!*(FARPROC*)StreamReader_RVA(0x21288))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x2128C) = ::GetProcAddress(hDll,"GetOEMCP");
	if(!*(FARPROC*)StreamReader_RVA(0x2128C))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21290) = ::GetProcAddress(hDll,"GetProcAddress");
	if(!*(FARPROC*)StreamReader_RVA(0x21290))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21294) = ::GetProcAddress(hDll,"LoadLibraryA");
	if(!*(FARPROC*)StreamReader_RVA(0x21294))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x21298) = ::GetProcAddress(hDll,"SetConsoleCtrlHandler");
	if(!*(FARPROC*)StreamReader_RVA(0x21298))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x2129C) = ::GetProcAddress(hDll,"FlushFileBuffers");
	if(!*(FARPROC*)StreamReader_RVA(0x2129C))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x212A0) = ::GetProcAddress(hDll,"Sleep");
	if(!*(FARPROC*)StreamReader_RVA(0x212A0))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x212A4) = ::GetProcAddress(hDll,"SetFilePointer");
	if(!*(FARPROC*)StreamReader_RVA(0x212A4))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x212A8) = ::GetProcAddress(hDll,"MultiByteToWideChar");
	if(!*(FARPROC*)StreamReader_RVA(0x212A8))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x212AC) = ::GetProcAddress(hDll,"LCMapStringA");
	if(!*(FARPROC*)StreamReader_RVA(0x212AC))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x212B0) = ::GetProcAddress(hDll,"LCMapStringW");
	if(!*(FARPROC*)StreamReader_RVA(0x212B0))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x212B4) = ::GetProcAddress(hDll,"GetStringTypeA");
	if(!*(FARPROC*)StreamReader_RVA(0x212B4))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x212B8) = ::GetProcAddress(hDll,"GetStringTypeW");
	if(!*(FARPROC*)StreamReader_RVA(0x212B8))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x212BC) = ::GetProcAddress(hDll,"CompareStringA");
	if(!*(FARPROC*)StreamReader_RVA(0x212BC))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x212C0) = ::GetProcAddress(hDll,"CompareStringW");
	if(!*(FARPROC*)StreamReader_RVA(0x212C0))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x212C4) = ::GetProcAddress(hDll,"SetEnvironmentVariableA");
	if(!*(FARPROC*)StreamReader_RVA(0x212C4))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x212C8) = ::GetProcAddress(hDll,"CloseHandle");
	if(!*(FARPROC*)StreamReader_RVA(0x212C8))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x212CC) = ::GetProcAddress(hDll,"SetStdHandle");
	if(!*(FARPROC*)StreamReader_RVA(0x212CC))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x212D0) = ::GetProcAddress(hDll,"IsValidLocale");
	if(!*(FARPROC*)StreamReader_RVA(0x212D0))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x212D4) = ::GetProcAddress(hDll,"IsValidCodePage");
	if(!*(FARPROC*)StreamReader_RVA(0x212D4))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x212D8) = ::GetProcAddress(hDll,"GetLocaleInfoA");
	if(!*(FARPROC*)StreamReader_RVA(0x212D8))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x212DC) = ::GetProcAddress(hDll,"EnumSystemLocalesA");
	if(!*(FARPROC*)StreamReader_RVA(0x212DC))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x212E0) = ::GetProcAddress(hDll,"GetUserDefaultLCID");
	if(!*(FARPROC*)StreamReader_RVA(0x212E0))
		return FALSE;
	*(FARPROC*)StreamReader_RVA(0x212E4) = ::GetProcAddress(hDll,"GetLocaleInfoW");
	if(!*(FARPROC*)StreamReader_RVA(0x212E4))
		return FALSE;

	*(FARPROC*)&fInitData = (FARPROC)&StreamReader_InitData[0];
	fInitData(StreamReader_RVA);

	VirtualProtect(StreamReader_text,sizeof(StreamReader_text),oldProtect,&oldProtect);

	*(FARPROC*)&StreamReader_DllEntryPoint = (FARPROC)StreamReader_RVA(0x839D);
	return TRUE;
}

BOOL StreamReader_LoadLibrary()
{
	return StreamReader_DllEntryPoint(g_hStreamReader, DLL_PROCESS_ATTACH, 0);
}

BOOL StreamReader_FreeLibrary()
{
	return StreamReader_DllEntryPoint(g_hStreamReader, DLL_PROCESS_DETACH, 0);
}

FARPROC StreamReader_GetProcAddress(LPCSTR lpProcName)
{
	if(lstrcmpA(lpProcName,"H264_PARSER_Destroy")==0)
		return (FARPROC)StreamReader_RVA(0x121C);
	if(lstrcmpA(lpProcName,"H264_PARSER_InputData")==0)
		return (FARPROC)StreamReader_RVA(0x10D2);
	if(lstrcmpA(lpProcName,"H264_PARSER_Reset")==0)
		return (FARPROC)StreamReader_RVA(0x112C);
	if(lstrcmpA(lpProcName,"H264_PARSER_GetNextFrame")==0)
		return (FARPROC)StreamReader_RVA(0x1212);
	if(lstrcmpA(lpProcName,"H264_PARSER_GetNextKeyFrame")==0)
		return (FARPROC)StreamReader_RVA(0x1244);
	if(lstrcmpA(lpProcName,"H264_PARSER_GetStreamType")==0)
		return (FARPROC)StreamReader_RVA(0x1104);
	if(lstrcmpA(lpProcName,"H264_PARSER_Create")==0)
		return (FARPROC)StreamReader_RVA(0x107D);
	if(lstrcmpA(lpProcName,"H264_PARSER_CreateEx")==0)
		return (FARPROC)StreamReader_RVA(0x105A);

	return NULL;
}
