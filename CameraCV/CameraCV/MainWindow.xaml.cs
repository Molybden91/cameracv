﻿//IntroScan Technology\\
using System;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.ComponentModel;
using System.Net;
using System.Net.Sockets;
using Emgu.CV;
using System.Windows.Threading;
using System.Threading.Tasks;
using Emgu.CV.UI;
using Emgu.CV.CvEnum;
using System.Diagnostics;
using System.IO;

//rtsp://192.168.88.1:554/user=admin_password=tlJwpbo6_channel=1_stream=0.sdp?real_stream  для подключения через MikroTik
//rtsp://192.168.1.21:554/user=admin_password=tlJwpbo6_channel=1_stream=0.sdp?real_stream  для подключения  напрямую

namespace CameraCV
{
    public partial class MainWindow : System.Windows.Window
    {
        #region Private Variable
        private bool recording;
        private VideoWriter _recordFrame;
        private VideoCapture _capture = null;
        private bool _captureInProgress;
        private Mat _frame;
        public delegate void MyDelegate(Mat frame);
        private MyDelegate del;
        private string currentUrl="";
        private int Pcount = 0;
        private RtspServer server;
        private IntPtr pictureBoxHandle;
        private string urlForConnection;
        private int videoWidth;
        private int videoHeight;
        private int videoFPS;
        private string currentFileName;
        #endregion
        public MainWindow()
        {
            InitializeComponent();
            Closing += OnWindowCloseEvent;
            textBoxForSimulatedIP.Text = GetLocalIpAddress();
            del = new MyDelegate(FrameDisplaying);
        }
        public void FrameDisplaying(Mat frame)
        {
            (WinFormsHostForImageBox.Child as ImageBox).Image = frame;
        }
        #region Import functions from unmanaged DLL for camera.
        [DllImport("CamVideo.dll")]
        public static extern int RunVideo([MarshalAs(UnmanagedType.LPStr)] string IpAddr, int port, [MarshalAs(UnmanagedType.LPStr)] string login, [MarshalAs(UnmanagedType.LPStr)] string password,IntPtr out_window);
        [DllImport("CamVideo.dll")]
        public static extern int StopVideo(IntPtr hwnd);
        [DllImport("CamVideo.dll")]
        public static extern int SetVideoParam(int nBrightness, int nContrast, int nSaturation, int nHue);
        [DllImport("CamVideo.dll")]
        public static extern int GetVideoParam(ref int nBrightness, ref int nContrast, ref int nSaturation, ref int nHue);
        [DllImport("CamVideo.dll")]
        public static extern int SetStreamVParams(IntPtr hwnd, int nBrightness, int nContrast, int nSaturation, int nHue);
        [DllImport("CamVideo.dll")]
        public static extern int GetStreamVParams(IntPtr hwnd, ref int nBrightness, ref int nContrast, ref int nSaturation, ref int nHue);
        [DllImport("CamVideo.dll")]
        public static extern int SavePicture(IntPtr hwnd, [MarshalAs(UnmanagedType.LPStr)] string sFileName);
        [DllImport("CamVideo.dll")]
        public static extern int StartRecord(IntPtr hwnd, [MarshalAs(UnmanagedType.LPStr)] string sFileName);
        [DllImport("CamVideo.dll")]
        public static extern int StopRecord(IntPtr hwnd);
        #endregion
        #region Application Window Events
        // Action that will be performed after application window closure
        private void OnWindowCloseEvent(object sender, CancelEventArgs e) {
            if (server != null)
            {
                server.StopListen();
                server.Dispose();
            }
            Environment.Exit(Environment.ExitCode);
        }
        #endregion

        #region Capturing Process
        // Main method for camera connetion. VideoCapture class creation, camera/server existence checking.
        public void ConnectCamera()
        {
            urlForConnection = "rtsp://" + IPtextBox.Text + ":" + PortTextBox.Text + "/user=" + LoginTextBox.Text + "_password=" + PasswordTextBox.Text + "_channel=1_stream=" + StreamTextBox.Text + ".sdp?real_stream";
            try
            {
                RtspClientExample.RTSPClient client = new RtspClientExample.RTSPClient();
                client.Connect(urlForConnection, RtspClientExample.RTSPClient.RTP_TRANSPORT.TCP);
                if (server!=null && IPtextBox.Text == textBoxForSimulatedIP.Text && PortTextBox.Text == textBoxForSimulatedPort.Text)
                {
                    _capture = new VideoCapture(urlForConnection);
                }
                else
                {
                    this.pictureBoxHandle = (WinFormsHostForImageBox.Child as PictureBox).Handle;
                    int result = RunVideo(IPtextBox.Text, Convert.ToInt32(PortTextBox.Text), LoginTextBox.Text, PasswordTextBox.Text, pictureBoxHandle);
                    SetVideoParam(Convert.ToInt32(sliderBr.Value), Convert.ToInt32(sliderContrast.Value), Convert.ToInt32(sliderSaturation.Value), Convert.ToInt32(sliderHue.Value));
                    currentUrl = "rtsp://" + IPtextBox.Text + ":" + PortTextBox.Text;
                }
            }catch (NullReferenceException exception)
            {
                infoBox1.Text = "Stopped";
                System.Windows.Forms.MessageBox.Show(exception.Message + urlForConnection);
            }
            if (_capture != null)
            {
                currentUrl = "rtsp://" + IPtextBox.Text + ":" + PortTextBox.Text;
                _frame = new Mat();
                _capture.ImageGrabbed += ProcessFrame;
                _capture.Pause();
                recording = false;
                videoWidth = _capture.Width;
                videoHeight = _capture.Height;
                videoFPS = Convert.ToInt32(_capture.GetCaptureProperty(CapProp.Fps));
                SetPauseImage();
            }
        }
        public void ProcessFrame(object sender, EventArgs arg)
        {
            DateTime start = DateTime.Now;
            if (_capture != null && _capture.Ptr != IntPtr.Zero)
            {
                GC.Collect();
                if (_capture.Retrieve(_frame))
                {
                    try
                    {
                        Dispatcher.Invoke(del, DispatcherPriority.Send, new object[] { _frame });
                    }
                    catch (TaskCanceledException exception)
                    {
                        _capture.Dispose();
                    }
                    if (recording && _recordFrame != null)
                    {
                        _recordFrame.Write(_frame);
                    }
                }
            }
            DateTime end = DateTime.Now;
        }
        #endregion

        #region Button Click Actions
        //Action for play/pause button
        private void captureButton_Click(object sender, RoutedEventArgs e)
        {
            if (server!= null && IPtextBox.Text == textBoxForSimulatedIP.Text && PortTextBox.Text == textBoxForSimulatedPort.Text)
            {
                if (!currentUrl.Equals("rtsp://" + IPtextBox.Text + ":" + PortTextBox.Text) && _capture != null)
                {
                    _capture.Stop();
                    _capture = null;
                    infoBox1.Text = "";
                }
                if ((_capture == null || !_captureInProgress) && infoBox1.Text != "Pause")
                {
                    infoBox1.Text = "Try to obtain Data";
                    ConnectCamera();
                }
                if (_capture != null)
                {
                    if (_captureInProgress)
                    {
                        SetStartImage();
                        infoBox1.Text = "Pause";
                        TextBoxConsist(true);
                        _capture.Pause();
                    }
                    else
                    {
                        SetPauseImage();
                        TextBoxConsist(false);
                        _capture.Start();
                        infoBox1.Text = "Running           " + urlForConnection;
                    }
                    _captureInProgress = !_captureInProgress;
                }
            }
            else {
                if (pictureBoxHandle == IntPtr.Zero)
                {
                    ConnectCamera();
                    infoBox1.Text = "Running           " + currentUrl;
                    TextBoxConsist(false);
                    SetPauseImage();
                }
                else
                {
                    StopVideo(pictureBoxHandle);
                    pictureBoxHandle = IntPtr.Zero;
                    TextBoxConsist(true);
                    SetStartImage();
                    infoBox1.Text = "Stopped";
                }
                _captureInProgress = !_captureInProgress;
            }
        }

        //Action for SetLocalIp Button
        private void SetLocalIP_Click(object sender, RoutedEventArgs e)
        {
            IPtextBox.Text = GetLocalIpAddress();
        }

        //Action for Default Button
        private void DefaultButton_Click(object sender, RoutedEventArgs e)
        {
            sliderBr.Value = 60;
            sliderContrast.Value = 60;
            sliderSaturation.Value = 60;
            sliderHue.Value = 60;
            SetVideoParam(Convert.ToInt32(sliderBr.Value), Convert.ToInt32(sliderContrast.Value), Convert.ToInt32(sliderSaturation.Value), Convert.ToInt32(sliderHue.Value));
        }
 
        //Action for photo Button. Allow to make snapshot.    
        private void photoButton_Click(object sender, RoutedEventArgs e)
        {
            if ( _captureInProgress)
            {
                var dialog = new Microsoft.Win32.SaveFileDialog { Filter = "PNG|*.png" };
                if (dialog.ShowDialog() == true)
                {
                    if (IPtextBox.Text == textBoxForSimulatedIP.Text && PortTextBox.Text == textBoxForSimulatedPort.Text && server != null && _capture!=null) {
                        _frame.Save(dialog.FileName);
                    }
                    SavePicture(pictureBoxHandle,dialog.FileName);
                    Pcount++;
                    photoCount.Content = Pcount;
                }
            }
            else
            {
                System.Windows.MessageBox.Show("Error to take a snapshot!");
            }
        }
        
        //Action for video camera(video recording) Button.
        private void VideoCameraButton_Click(object sender, RoutedEventArgs e)
        {
            if (_captureInProgress)
            {
                if (recording == false)
                {
                    recording = true;
                    if (server!=null && _capture != null)
                    {
                        System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog
                        {
                            DefaultExt = ".avi",
                            AddExtension = true,
                            FileName = "ScannerVideoTest"
                        };
                        DialogResult dialogResult = dialog.ShowDialog();
                        _recordFrame = new VideoWriter(dialog.FileName, videoFPS, new System.Drawing.Size(videoWidth, videoHeight), true);
                    }
                    else {
                        System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog
                        {
                            DefaultExt = ".h264",
                            AddExtension = true,
                            FileName = "ScannerVideoTest"
                        };
                        DialogResult dialogResult = dialog.ShowDialog();
                        currentFileName = dialog.FileName;
                        StartRecord(pictureBoxHandle, dialog.FileName);
                    }
                    SetRecImage();
                }
                else
                {
                    if (recording == true)
                    {
                        recording = false;
                        recButtonImage.Source = null;
                        StopRecord(pictureBoxHandle);
                        _recordFrame = null;

                        Process process = new Process();
                        process.StartInfo.RedirectStandardOutput = true;
                        process.StartInfo.RedirectStandardError = true;
                        process.StartInfo.FileName = "C:\\Users\\am\\Documents\\CameraCV\\CameraCV\\CameraCV\\bin\\Debug\\ffmpeg.exe";
                        process.StartInfo.Arguments = "-framerate 25 -i "+currentFileName+" -c copy "+ currentFileName.Substring(0,currentFileName.LastIndexOf('.'))+".mp4";
                        process.StartInfo.UseShellExecute = false;
                        process.StartInfo.CreateNoWindow = true;

                        process.Start();
                    }
                }
            }
            else
            {
                System.Windows.MessageBox.Show("Error to take a video!");
            }
        }

        //Button for simulation startup
        private void StartSimulation_Click(object sender, RoutedEventArgs args) {
            createRTSP();
        }
        #endregion

        #region Sliders Value Changed Actions
        private void ColorParameters_ValueChanged(object sender, RoutedEventArgs e)
        {
            if (infoBox1.Text.Contains("Running"))
            {
                SetVideoParam(Convert.ToInt32(sliderBr.Value), Convert.ToInt32(sliderContrast.Value), Convert.ToInt32(sliderSaturation.Value), Convert.ToInt32(sliderHue.Value));
            }
        }
        #endregion

        #region Static Methods
        //Obtain local IP (not localhost like 127.0.0.1)
        private static string GetLocalIpAddress()
        {
            string localIP = "";
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }
        #endregion

        #region Auxiliary Methods
        //Settings of enabling UI elements connected with video capturing
        private void TextBoxConsist(bool consist)
        {
            StreamTextBox.IsEnabled = consist;
            PortTextBox.IsEnabled = consist;
            IPtextBox.IsEnabled = consist;
            LoginTextBox.IsEnabled = consist;
            PasswordTextBox.IsEnabled = consist;
            SetLocalIP.IsEnabled = consist;
        }
        //Method that allow to create RTSP server for simulation using SharpRTSP libraty and RTSPServer example
        private void createRTSP()
        {
            if (server == null)
            {
                try
                {
                    server = new RtspServer(Convert.ToInt32(textBoxForSimulatedPort.Text), "admin", "");
                    server.StartListen();
                    StartSimulationButton.Content = "Stop Simulation";
                    textBoxForSimulatedPort.IsEnabled = false;
                    SimulationImage.Source = new BitmapImage(new Uri("/CameraCV;component/images/on.jpg", UriKind.Relative));
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    System.Windows.MessageBox.Show("The port number must be between: " + System.Net.IPEndPoint.MinPort + " and " + System.Net.IPEndPoint.MaxPort);
                    IPtextBox.Text = "554";
                }
            }
            else
            {
                server.StopListen();
                server.Dispose();
                server = null;
                StartSimulationButton.Content = "Create Simulated Cam";
                textBoxForSimulatedPort.IsEnabled = true;
                SimulationImage.Source = new BitmapImage(new Uri("/CameraCV;component/images/off.jpg", UriKind.Relative));
                if (IPtextBox.Text == textBoxForSimulatedIP.Text && PortTextBox.Text == textBoxForSimulatedPort.Text)
                {
                    SetStartImage();
                    _capture.Stop();
                    _capture = null;
                    TextBoxConsist(true);
                    infoBox1.Text = "Stopped";
                    _captureInProgress = !_captureInProgress;
                }
            }
        }
        #endregion

        #region Set Button Properties
        //Set Pause image to play/pause Button
        private void SetPauseImage()
        {
            BitmapImage PauseImage = new BitmapImage();
            PauseImage.BeginInit();
            PauseImage.UriSource = new Uri("/CameraCV;component/images/pause.PNG", UriKind.Relative);
            PauseImage.EndInit();
            captureButtonImage.Source = PauseImage;
        }
        //Set Start image to play/pause Button
        private void SetStartImage()
        {
            BitmapImage StartImage = new BitmapImage();
            StartImage.BeginInit();
            StartImage.UriSource = new Uri("/CameraCV;component/images/play.PNG", UriKind.Relative);
            StartImage.EndInit();
            captureButtonImage.Source = StartImage;
        }
        //Set Recors image to video camera(video recording) Button
        private void SetRecImage()
        {
            BitmapImage RecImage = new BitmapImage();
            RecImage.BeginInit();
            RecImage.UriSource = new Uri("/CameraCV;component/images/rec.PNG", UriKind.Relative);
            RecImage.EndInit();
            recButtonImage.Source = RecImage;
        }       
        #endregion   
    }
}
